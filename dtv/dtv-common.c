/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "exec/memory.h"
#include "dtv-common.h"

hwaddr dtv_memory_get_addr(MemoryRegion *mr)
{
    hwaddr a = mr->addr;

    if(mr->container)
    	a += dtv_memory_get_addr(mr->container);

    return a;
}

bool dtv_memory_is_direct(MemoryRegion *mr)
{
    if(memory_region_is_ram(mr))
    	return true;

    if(memory_region_is_romd(mr))
    	return true;

    if(mr->alias && dtv_memory_is_direct(mr->alias))
    	return true;

    return false;
}

