/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <glib.h>
#include "exec/memory.h"
#include "dtv-init.h"
#include "dtv-common.h"

static GHashTable *mrtable = NULL;

static void filter_add(MemoryRegion *mr, uint64_t addr, uint64_t value, unsigned size, char type, int64_t clock)
{
    if(!g_hash_table_lookup(mrtable, mr))
	return;
    
    dtv_trace_add(mr, addr, value, size, type, clock);
}

void dtv_memory_region_ops_read(int64_t clock, void *mr, uint64_t addr, uint64_t value, unsigned size)
{
    filter_add(mr, addr, value, size, 'L', clock);
}

void dtv_memory_region_ops_write(int64_t clock, void *mr, uint64_t addr, uint64_t value, unsigned size)
{
    filter_add(mr, addr, value, size, 'S', clock);
}

void dtv_filter_mr_add(MemoryRegion *mr)
{
    g_hash_table_replace(mrtable, mr, mr);
}

void dtv_filter_mr_remove(MemoryRegion *mr)
{
    g_hash_table_remove(mrtable, mr);
}

void dtv_filter_init(void)
{
    mrtable = g_hash_table_new(NULL, NULL);
}

