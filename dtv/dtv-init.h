/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __DTV_INIT_H__
#define __DTV_INIT_H__

#include <stdint.h>

void dtv_init(void);
void dtv_main(void);

void dtv_memory_region_ops_read(int64_t clock, void *mr, uint64_t addr, uint64_t value, unsigned size);
void dtv_memory_region_ops_write(int64_t clock, void *mr, uint64_t addr, uint64_t value, unsigned size);

#endif
