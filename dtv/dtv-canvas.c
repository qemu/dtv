/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <goocanvas.h>
#include "dtv-canvas.h"

struct state {
    GtkAdjustment *hadj;
    GtkAdjustment *vadj;
    gdouble lastx;
    gdouble lasty;
    GooCanvasItem *head;
};

static gboolean cb_button_event(GtkWidget *widget, GdkEvent *event, struct state *state)
{
    GdkWindow *window = gtk_widget_get_window(widget);
    if(event->type == GDK_BUTTON_PRESS) {
    	state->lastx = event->button.x_root;
	state->lasty = event->button.y_root;
    	if(window)
	    gdk_window_set_cursor(window, gdk_cursor_new(GDK_FLEUR));

	goo_canvas_grab_focus(GOO_CANVAS (widget), goo_canvas_get_root_item(GOO_CANVAS (widget)));
    }
    if(event->type == GDK_BUTTON_RELEASE) {
    	if(window)
	    gdk_window_set_cursor(window, NULL);
    }
    return FALSE;
}

static void adjustment_step_value(GtkAdjustment *adj, gdouble step)
{
    gdouble value = gtk_adjustment_get_value(adj);
    gdouble lower = gtk_adjustment_get_lower(adj);
    gdouble upper = gtk_adjustment_get_upper(adj);
    gdouble page  = gtk_adjustment_get_page_size(adj);
    gdouble maxval = upper - page;

    value += step;
    value = (value > maxval) ? maxval : value;
    gtk_adjustment_set_value(adj, value);
}

static gboolean cb_motion_event(GtkWidget *widget, GdkEvent *event, struct state *state)
{
    gdouble dx;
    gdouble dy;
    if(event->motion.state & GDK_BUTTON1_MASK) {
        dx = event->motion.x_root - state->lastx;
	dy = event->motion.y_root - state->lasty;

	if(((gint)dx) != 0)
	    adjustment_step_value(state->hadj, -dx);
	if(((gint)dy) != 0)
	    adjustment_step_value(state->vadj, -dy);

	state->lastx = event->motion.x_root;
	state->lasty = event->motion.y_root;
    }

    return FALSE;
}

static gboolean cb_key_event(GtkWidget *widget, GdkEvent *event, struct state *state)
{
    if(event->type == GDK_KEY_PRESS) {
    	switch(event->key.keyval) {
	    case GDK_KEY_Left:
	    	adjustment_step_value(state->hadj, -gtk_adjustment_get_step_increment(state->hadj));
	    	break;
	    case GDK_KEY_Up:
	    	adjustment_step_value(state->vadj,  gtk_adjustment_get_step_increment(state->vadj));
	    	break;
	    case GDK_KEY_Right:
	    	adjustment_step_value(state->hadj,  gtk_adjustment_get_step_increment(state->hadj));
		break;
	    case GDK_KEY_Down:
	    	adjustment_step_value(state->vadj, -gtk_adjustment_get_step_increment(state->vadj));
	    	break;
	    case GDK_KEY_Page_Up:
	    	adjustment_step_value(state->vadj, -gtk_adjustment_get_page_size(state->vadj));
	    	break;
	    case GDK_KEY_Page_Down:
	    	adjustment_step_value(state->vadj,  gtk_adjustment_get_page_size(state->vadj));
	    	break;
	    default:
	    	return FALSE;
	    	break;
	}
    }
    return TRUE;
}

static void cb_value_changed(GtkAdjustment *adj, struct state *state)
{
    GooCanvas *canvas;
    gdouble x = 0.;
    gdouble y;
    if(state->head) {
    	canvas = goo_canvas_item_get_canvas(state->head);
	y = gtk_adjustment_get_value(adj);
	goo_canvas_convert_from_pixels(canvas, &x, &y);
	g_object_set(G_OBJECT (state->head), "y", y, NULL);
	goo_canvas_update(canvas);
    }
}

static void cb_set_adjustments(GooCanvas *canvas, GtkAdjustment *hadj, GtkAdjustment *vadj, struct state *state)
{
    state->hadj = hadj;
    state->vadj = vadj;
    if(vadj)
    	g_signal_connect(G_OBJECT (vadj), "value-changed", G_CALLBACK (cb_value_changed), state);
}

static void cb_destroy(GtkObject *object, struct state *state)
{
    g_free(state);
}

GtkWidget *dtv_canvas_new(void)
{
    GtkWidget *view;
    GtkWidget *canvas;
    struct state *state = g_new0(struct state, 1);

    view = gtk_scrolled_window_new(NULL, NULL);

    canvas = goo_canvas_new();
    g_signal_connect(G_OBJECT (canvas), "set-scroll-adjustments", G_CALLBACK (cb_set_adjustments), state);
    gtk_container_add(GTK_CONTAINER (view), canvas);
    g_signal_connect(G_OBJECT (canvas), "button-press-event", G_CALLBACK (cb_button_event), state);
    g_signal_connect(G_OBJECT (canvas), "button-release-event", G_CALLBACK (cb_button_event), state);
    g_signal_connect(G_OBJECT (canvas), "motion-notify-event", G_CALLBACK (cb_motion_event), state);
    g_signal_connect(G_OBJECT (canvas), "key-press-event", G_CALLBACK (cb_key_event), state);
    g_signal_connect(G_OBJECT (canvas), "destroy", G_CALLBACK (cb_destroy), state);

    g_object_set_data(G_OBJECT (canvas), "state", state);

    return view;
}

GooCanvas *dtv_canvas_get_canvas(GtkWidget *widget)
{
	GtkWidget *canvas = gtk_bin_get_child(GTK_BIN (widget));
	return GOO_CANVAS (canvas);
}

GooCanvasItem *dtv_canvas_new_root(GooCanvas *canvas)
{
	GooCanvasItem *root = goo_canvas_group_new(NULL, "can-focus", TRUE, NULL);
	goo_canvas_set_root_item(canvas, root);
	return root;
}

void dtv_canvas_reset_bounds(GooCanvas *canvas)
{
	GooCanvasItem *root = goo_canvas_get_root_item(canvas);
	GooCanvasBounds b;
	goo_canvas_item_get_bounds(root, &b);
	goo_canvas_set_bounds(canvas, b.x1-7, b.y1, b.x2+7, b.y2+7);
}

void dtv_canvas_set_head(GooCanvas *canvas, GooCanvasItem *head)
{
	GooCanvasItem *root;
	struct state *state = g_object_get_data(G_OBJECT (canvas), "state");
	if(state) {
		if(state->head)
			goo_canvas_item_remove(state->head);
		state->head = head;

		root = goo_canvas_get_root_item(canvas);
		goo_canvas_item_add_child(root, head, -1);
	}
}

