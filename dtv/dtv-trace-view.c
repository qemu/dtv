/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <gtk/gtk.h>
#include <goocanvas.h>
#include "dtv-common.h"
#include "dtv-canvas.h"
#include "dtv.h"

static GooCanvas *canvas = NULL;

GtkWidget *dtv_construct_trace_view(void)
{
	GtkWidget *view;

	view = dtv_canvas_new();
	canvas = dtv_canvas_get_canvas(view);

	return view;
}

struct offsets {
	GTree *tree;
	gint last;
	gint xpos;
};

static gint func_compare_offset(gconstpointer a, gconstpointer b)
{
	return (gint)a - (gint)b;
}

static gboolean func_foreach_offset(gpointer key, gpointer value, gpointer data)
{
	gint off = (gint)key;
	gint pos = (gint)value;
	struct offsets *offs = (struct offsets*)data;

	if((offs->last > 0) && (off > (offs->last + 4)))
		offs->xpos++;
	g_tree_insert(offs->tree, key, GINT_TO_POINTER (offs->xpos));
	offs->xpos++;
	offs->last = off;
	return FALSE;
}

static GTree* compute_offset_positions(struct trace *trace)
{
	struct trace *t = trace;
	struct offsets offs;
	gint off;

	offs.tree = g_tree_new(func_compare_offset);
	offs.last = -1;
	offs.xpos = 0;

	while(t) {
		off = ((gint)t->addr) & ~3;
		g_tree_insert(offs.tree, GINT_TO_POINTER (off), 0);
		if(t->next && t->mr != t->next->mr)
			break;
		t = t->next;
	}
	g_tree_foreach(offs.tree, func_foreach_offset, &offs);
	return offs.tree;
}

static void item_new_value(GooCanvasItem *parent, gdouble x, gdouble y, gdouble w, struct trace *trace)
{
	GooCanvasItem *group;
	gint s;
	gint p;
	gint q;
	gchar text[32];
	const gchar *wg = "#DDD";
	const gchar *bg = "#AAA";
	const gchar *fg = "#333";

	if(trace->size != 4) {
		s = 2 * (trace->addr % 4);
		p = 2 * trace->size;
		q = ((8-p-s) > 0) ? (8-p-s) : 0;
		g_snprintf(text, sizeof(text), "%*s%0*llx%*s", q, "", p, trace->value, s, "");
	}
	else
		g_snprintf(text, sizeof(text), "%08llx", trace->value);

	if(trace->type == 'L') {
		wg = "#EFF";
		bg = "#AEE";
		fg = "#377";
	}
	if(trace->type == 'S') {
		wg = "#FEF";
		bg = "#EBE";
		fg = "#737";
	}

	group = goo_canvas_group_new(parent, NULL);
	goo_canvas_rect_new(group, 0., y, w, 20, "line-width", 0., "fill-color", wg, NULL);
	goo_canvas_rect_new(group, x, y, 70, 20, "line-width", 0., "fill-color", bg, NULL);
	goo_canvas_text_new(group, text, x + 35, y + 10, 0, GTK_ANCHOR_CENTER,
					 "font", "Monospace 10", "stroke-color", "#377", NULL);
}

static void item_new_offset(GooCanvasItem *parent, gint off, gdouble x)
{
	GooCanvasItem *group;
	gchar text[32];

	g_snprintf(text, sizeof(text), "%x", off);
	group = goo_canvas_group_new(parent, NULL);
	goo_canvas_rect_new(group, x, 0., 70, 20, "line-width", 0., "fill-color", "#EEA", NULL);
	goo_canvas_text_new(group, text, x + 35, 10, 0, GTK_ANCHOR_CENTER,
					 "font", "Monospace 10", "stroke-color", "#773", NULL);
}

static gboolean func_head_add_offsets(gpointer key, gpointer value, gpointer data)
{
	GooCanvasItem *parent = (GooCanvasItem*)data;
	gint off = (gint)key;
	gint pos = (gint)value;
	gdouble x = 70. * pos;
	item_new_offset(parent, off, x);
	return FALSE;
}

static GooCanvasItem* item_new_head(GooCanvasItem *root, GTree *offpos)
{
	GooCanvasItem *group;
	GooCanvasItem *offse;
	GooCanvasItem *back;
	GooCanvasBounds b;
	group = goo_canvas_group_new(root, NULL);
	offse = goo_canvas_group_new(group, NULL);
	g_tree_foreach(offpos, func_head_add_offsets, offse);

	goo_canvas_item_get_bounds(offse, &b);
	back = goo_canvas_rect_new(group, b.x1, b.y1, b.x2-b.x1, b.y2-b.y1, "line-width", 0., "fill-color", "#DD8", NULL);
	goo_canvas_item_lower(back, offse);
	return group;
}

void dtv_trace_view_set_trace(struct trace *trace)
{
	GooCanvasItem *root = dtv_canvas_new_root(canvas);
	GooCanvasItem *back = goo_canvas_group_new(root, NULL);
	GooCanvasItem *fron = goo_canvas_group_new(root, NULL);
	GTree *offpos = compute_offset_positions(trace);
	GooCanvasBounds b;

	dtv_canvas_set_head(canvas, item_new_head(fron, offpos));
	goo_canvas_item_get_bounds(fron, &b);

	struct trace *t = trace;
	gint off;
	gint y = 20;
	gint x = 0;
	gint w = b.x2 - b.x1;

	while(t) {
		off = ((gint)t->addr) & ~3;
		x = 70 * (gint)g_tree_lookup(offpos, GINT_TO_POINTER (off));
		item_new_value(back, x, y, w, t);
		y += 20;
		if(t->next && t->mr != t->next->mr)
			break;
		t = t->next;
	}

	g_tree_destroy(offpos);
	dtv_canvas_reset_bounds(canvas);
}

