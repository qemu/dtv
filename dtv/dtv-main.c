/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <gtk/gtk.h>
#include "qmp-commands.h"
#include "dtv-init.h"
#include "dtv.h"

static void cb_reset(GtkButton *b, gpointer data)
{
    dtv_trace_destroy();
    qmp_system_reset(NULL);
}

static void cb_continue(GtkButton *b, gpointer data)
{
    qmp_cont(NULL);
}

static void cb_stop(GtkButton *b, gpointer data)
{
    qmp_stop(NULL);
}


static GtkWidget *construct_topbar(void)
{
    GtkWidget *bar;
    GtkWidget *but;

    bar = gtk_hbox_new(FALSE, 6);


    but = gtk_button_new_with_label("reset");
    gtk_box_pack_start(GTK_BOX (bar), but, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT (but), "clicked", G_CALLBACK (cb_reset), NULL);

    but = gtk_button_new_with_label("cont");
    gtk_box_pack_start(GTK_BOX (bar), but, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT (but), "clicked", G_CALLBACK (cb_continue), NULL);

    but = gtk_button_new_with_label("stop");
    gtk_box_pack_start(GTK_BOX (bar), but, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT (but), "clicked", G_CALLBACK (cb_stop), NULL);

    return bar;
}

void dtv_main(void)
{
    GtkWidget *wtop;
    GtkWidget *box;
    GtkWidget *bar;
    GtkWidget *notebook;
    GtkWidget *page;

    if(!gtk_init_check(NULL, NULL))
    	return;

    wtop = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW (wtop), "DTV");
    gtk_window_maximize(GTK_WINDOW (wtop));

    box = gtk_vbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER (wtop), box);

    bar = construct_topbar();
    gtk_box_pack_start(GTK_BOX (box), bar, FALSE, FALSE, 0);

    notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX (box), notebook, TRUE, TRUE, 0);

    page = dtv_construct_memory_page();
    gtk_notebook_append_page(GTK_NOTEBOOK (notebook), page, NULL);
    gtk_notebook_set_tab_label_text(GTK_NOTEBOOK (notebook), page, "memory");

    page = dtv_construct_view_page();
    gtk_notebook_append_page(GTK_NOTEBOOK (notebook), page, NULL);
    gtk_notebook_set_tab_label_text(GTK_NOTEBOOK (notebook), page, "view");

    gtk_widget_show_all(wtop);
}

