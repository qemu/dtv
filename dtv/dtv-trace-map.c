/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <gtk/gtk.h>
#include <goocanvas.h>
#include "dtv-common.h"
#include "dtv-canvas.h"
#include "dtv.h"

static GooCanvas *canvas = NULL;
static GooCanvasItem *map = NULL;
static GTree *mrtree = NULL;
static struct {
	GooCanvasItem *pick;
	GooCanvasItem *item;
	struct trace *trace;
	gdouble ypos;
	gdouble height;
	gint offset;
	gint width;
} last;

static gint func_compare_region(gconstpointer a, gconstpointer b)
{
	MemoryRegion *ra = (MemoryRegion *)a;
	MemoryRegion *rb = (MemoryRegion *)b;
	hwaddr aa = dtv_memory_get_addr(ra);
	hwaddr ab = dtv_memory_get_addr(rb);
	return (aa - ab);
}

static gboolean func_compute_positions(gpointer key, gpointer val, gpointer data)
{
	GooCanvasItem *item = (GooCanvasItem *)val;
	gdouble *pos = (gdouble*)data;
	g_object_set(G_OBJECT (item), "x", *pos, NULL);
	*pos += last.offset;
	return FALSE;
}

static void compute_region_positions(GTree *regions)
{
	gdouble pos = 0.;
	g_tree_foreach(regions, func_compute_positions, &pos);
}

static void pick_reset_bounds(GooCanvasItem *item)
{
	GooCanvasItem *root = goo_canvas_get_root_item(canvas);
	GooCanvasBounds r;
	GooCanvasBounds b;
	gdouble x;
	gdouble y;
	gdouble w;
	gdouble h;

	if(!last.pick && item)
		last.pick = goo_canvas_rect_new(root, 0., 0., 0., 0.,
						"line-width", 1., "stroke-color", "#7A7",
						"fill-color-rgba", 0x77AA7733, NULL);
	goo_canvas_item_get_bounds(map, &r);
	x = r.x1 + 1;
	w = r.x2 - r.x1 - 2;
	if(w < (last.width - 2 - 2 * 7))
	    w = last.width - 2 - 2 * 7;
	if(item) {
		goo_canvas_item_get_bounds(item, &b);
		y = b.y1 + 1;
		h = b.y2 - b.y1 - 2;
		g_object_set(G_OBJECT (last.pick), "x", x, "y", y, "width", w, "height", h, NULL);
	}
	else if(last.pick)
		g_object_set(G_OBJECT (last.pick), "x", x, "width", w, NULL);

	dtv_canvas_reset_bounds(canvas);
}

static void compute_region_offset(GooCanvas *canvas)
{
	gint n = g_tree_nnodes(mrtree);
	gint o = 50;
	gdouble w = 0.;
	gdouble h = 0.;
	w = last.width;

	goo_canvas_convert_from_pixels(canvas, &w, &h);
	if(n > 1)
		o = (w - 50 - 10) / (n - 1);
	if(o < 0)
		o = 0;
	if(o > 50)
		o = 50;
	
	last.offset = o;
	compute_region_positions(mrtree);
	pick_reset_bounds(NULL);
}

static gboolean cb_size_allocate(GtkWidget *widget, GdkRectangle *allocation, gpointer data)
{
	last.width = allocation->width;
	compute_region_offset(GOO_CANVAS (widget));
	return FALSE;
}

static void map_remove_new(void)
{
	GooCanvasItem *root = goo_canvas_get_root_item(canvas);

	if(map)
		goo_canvas_item_remove(map);
	if(last.pick)
		goo_canvas_item_remove(last.pick);
	if(mrtree)
		g_tree_destroy(mrtree);

	map = goo_canvas_group_new(root, "font", "Monospace 8", NULL);
	last.pick   = NULL;
	last.item   = NULL;
	last.trace  = NULL;
	last.ypos   = 0.;
	last.height = 0.;
	mrtree = g_tree_new(func_compare_region);
}

GtkWidget *dtv_construct_trace_map(void)
{
	GtkWidget *view;

	view = dtv_canvas_new();
	gtk_scrolled_window_set_placement(GTK_SCROLLED_WINDOW (view), GTK_CORNER_TOP_RIGHT);

	canvas = dtv_canvas_get_canvas(view);
	gtk_widget_set_size_request(GTK_WIDGET (canvas), 270, -1);
	g_signal_connect(G_OBJECT (canvas), "size-allocate", G_CALLBACK (cb_size_allocate), NULL);

	map_remove_new();

	return view;
}

static const gchar *colorize_region(struct trace *trace, gchar color[5])
{
	hwaddr a = dtv_memory_get_addr(trace->mr);
	const gchar *palette = "AEABCDEF";
	guint r = (a >> 23) & 0xff;
	guint g = (a >> 15) & 0xff;
	guint b = (a >>  0) & 0xffff;
#define B3(a) ((a) & 7)
	r = B3(r) ^ B3(r>>3) ^ B3(r>>6);
	g = B3(g) ^ B3(g>>3) ^ B3(g>>6);
	b = B3(b) ^ B3(b>>3) ^ B3(b>>6) ^ B3(b>>9) ^ B3(b>>12) ^ B3(b>>15);
	color[1] = palette[B3(r)];
	color[2] = palette[B3(g)];
	color[3] = palette[B3(b)];
	return color;
}

static GooCanvasItem *map_get_group(struct trace *trace)
{
	GooCanvasItem *root = map;
	GooCanvasItem *item = g_tree_lookup(mrtree, trace->mr);
	gchar bg[5] = "#000";

	if(!item) {
		colorize_region(trace, bg);
		item = goo_canvas_group_new(root, "fill-color", bg, NULL);
		g_tree_insert(mrtree, trace->mr, item);
		compute_region_offset(canvas);
	}

	return item;
}

static gboolean cb_enter_notify(GooCanvasItem *item, GooCanvasItem *target, GdkEventCrossing *event, gpointer data)
{
	g_object_set(G_OBJECT (item), "line-width", 2.0, NULL);
	return FALSE;
}

static gboolean cb_leave_notify(GooCanvasItem *item, GooCanvasItem *target, GdkEventCrossing *event, gpointer data)
{
	g_object_set(G_OBJECT (item), "line-width", 0.0, NULL);
	return FALSE;
}

static gboolean cb_button_press(GooCanvasItem *item, GooCanvasItem *target, GdkEventButton *event, gpointer data)
{
	struct trace *t = (struct trace*)data;
	dtv_trace_view_set_trace(t);
	pick_reset_bounds(item);
	return TRUE;
}

void dtv_trace_map_add(struct trace *t)
{
	GooCanvasItem *col;
	GooCanvasItem *item;
	GooCanvasItem *rect;
	gchar text[32];
	gdouble x = 0.;
	gdouble y = last.ypos + last.height;
	gdouble w = 50.;
	gdouble h = 10.;

	col = map_get_group(t);

	rect = goo_canvas_group_new(col, NULL);
	g_signal_connect(G_OBJECT(rect), "enter-notify-event", G_CALLBACK (cb_enter_notify), NULL);
	g_signal_connect(G_OBJECT(rect), "leave-notify-event", G_CALLBACK (cb_leave_notify), NULL);
	g_signal_connect(G_OBJECT(rect), "button-press-event", G_CALLBACK (cb_button_press), t);

	item = goo_canvas_rect_new(rect, x, y, w, h, "line-width", 0., NULL);
	goo_canvas_text_new(rect, t->mr->name, w / 2, y, w, GTK_ANCHOR_NORTH, "fill-color", "#000",
							    "pointer-events", GOO_CANVAS_EVENTS_NONE, NULL);

	g_snprintf(text, sizeof(text), "%lld.%03lld", t->clock / 1000, (t->clock % 1000));
	goo_canvas_text_new(map, text, -10., y + h / 2, 0., GTK_ANCHOR_EAST, "font", "Monospace 8", NULL);

	last.ypos = y;
	last.item = item;
	last.trace = t;
}

void dtv_trace_map_update(void)
{
	struct trace *t = last.trace;
	gint count = 0;
	gdouble h;

	while(t) {
		if(t->next && t->mr != t->next->mr)
				break;
		count++;
		t = t->next;
	}

	h = 10. + count * 2.;
	g_object_set(G_OBJECT (last.item), "height", h, NULL);

	last.height = h;
	dtv_canvas_reset_bounds(canvas);
}

void dtv_trace_map_clear(void)
{
	map_remove_new();
}

