/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __DTV_CANVAS_H__
#define __DTV_CANVAS_H__

#include <gtk/gtk.h>
#include <goocanvas.h>

GtkWidget *dtv_canvas_new(void);
GooCanvas *dtv_canvas_get_canvas(GtkWidget *parent);
GooCanvasItem *dtv_canvas_new_root(GooCanvas *canvas);
void dtv_canvas_reset_bounds(GooCanvas *canvas);
void dtv_canvas_set_head(GooCanvas *canvas, GooCanvasItem *head);

#endif
