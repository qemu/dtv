/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "hw/boards.h"
#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/arm/allwinner-a10.h"
#include "device-common.h"

#define VEVM_RAMADDR 0x40000000

static struct arm_boot_info vevm_binfo;

static void vevm_init(MachineState *state)
{
    Object *obj;
    qemu_irq irq53;
    MemoryRegion *system;
    MemoryRegion *sysram;

    obj = object_new(TYPE_AW_A10);
    object_property_set_int(OBJECT(&AW_A10(obj)->timer),    32768, "clk0-freq", &error_abort);
    object_property_set_int(OBJECT(&AW_A10(obj)->timer), 24000000, "clk1-freq", &error_abort);
    object_property_set_int(OBJECT(&AW_A10(obj)->emac) ,        1,  "phy-addr", &error_abort);
    object_property_set_bool(obj, true, "realized", &error_abort);
    irq53 = AW_A10(obj)->irq[53];


    system = get_system_memory();
    sysram = g_new0(MemoryRegion, 1);
    memory_region_allocate_system_memory(sysram, NULL, "generic.ram", state->ram_size);
    memory_region_add_subregion(system, VEVM_RAMADDR, sysram);

    dummy_region_init(NULL, "mmc0", 0x01c0f000, 0x1000);
    dummy_region_init(NULL,  "pio", 0x01c20800,  0x400);
    dummy_region_init(NULL,  "sid", 0x01c23800,  0x400);
    dummy_region_init(NULL, "twi0", 0x01c2ac00,  0x400);

    ccm_device_init(0x01c20000);
    ve_device_init(0x01c0e000, irq53);


    vevm_binfo.ram_size		= state->ram_size;
    vevm_binfo.kernel_filename	= state->kernel_filename;
    vevm_binfo.kernel_cmdline	= state->kernel_cmdline;
    vevm_binfo.initrd_filename	= state->initrd_filename;
    vevm_binfo.loader_start	= VEVM_RAMADDR;
    arm_load_kernel(ARM_CPU(first_cpu), &vevm_binfo);
}

static void vevm_machine_init(MachineClass *mc)
{
    mc->desc = "vev machine for experiments.";
    mc->init = vevm_init;
    mc->max_cpus = 1;
}

DEFINE_MACHINE("dtv-vev", vevm_machine_init);

