/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "hw/sysbus.h"
#include "device-common.h"
/*
    This device doesn't aims to be accurate, only the minimum to make the
    software run.
*/
#define VE_SIZE 0x1000
#define VE_NREG ((VE_SIZE) / (sizeof(uint32_t)))
#define TYPE_VE "vev-ve"
#define VE(obj) OBJECT_CHECK(VEState, (obj), TYPE_VE)

typedef struct VEState {
    SysBusDevice parent;
    MemoryRegion iomem;
    qemu_irq	 irq;
    union {
	uint32_t val[VE_NREG];
	uint16_t vs2[0];
	uint8_t  vs1[0];
    } reg;
} VEState;

struct ve_reset_values {
    hwaddr	offset;
    uint32_t	value;
} ve_reset_values[] = {
    { 0x000, 0x00000007 },
    { 0x0f0, 0x16230000 },
};

#define VEREG(off) (ve->reg.val[(off)>>2])

static void ve_phy_dma_copy(hwaddr pdst, hwaddr psrc, hwaddr plen)
{
    void *buf;
    pdst = (pdst & 0x0fffffff) | 0x40000000;
    psrc = (psrc & 0x0fffffff) | 0x40000000;
    buf = cpu_physical_memory_map(psrc, &plen, 0);
    cpu_physical_memory_write(pdst, buf, plen);
    cpu_physical_memory_unmap(buf, plen, 0, plen);
}

static void ve_isp_do_dma_copy(VEState *ve)
{
    hwaddr plen;
    if((VEREG(0xa00) == VEREG(0xa04)) && (VEREG(0xa08) == 0x00080001)) {
	plen = 16 * 16 * (VEREG(0xa00) & 0x1fff) * ((VEREG(0xa00) >> 16) & 0x1fff);
	ve_phy_dma_copy(VEREG(0xa70), VEREG(0xa78), plen);
	ve_phy_dma_copy(VEREG(0xa74), VEREG(0xa7c), plen/2);
	qemu_irq_raise(ve->irq);
    }
}

void ve_device_init(hwaddr addr, qemu_irq irq)
{
    DeviceState *dev;

    dev = qdev_create(NULL, TYPE_VE);
    qdev_init_nofail(dev);
    sysbus_mmio_map(SYS_BUS_DEVICE(dev), 0, addr);
    sysbus_connect_irq(SYS_BUS_DEVICE(dev), 0, irq);
}

static uint64_t ve_data_read(VEState *ve, hwaddr addr, unsigned size)
{
    uint64_t data = 0;
    switch(size) {
	case 4: data = ve->reg.val[addr>>2]; break;
	case 2: data = ve->reg.vs2[addr>>1]; break;
	case 1: data = ve->reg.vs1[addr>>0]; break;
	default:
	    break;
    }
    return data;
}

static uint64_t ve_data_write(VEState *ve, hwaddr addr, uint64_t data, unsigned size)
{
    switch(size) {
	case 4: ve->reg.val[addr>>2] = data; break;
	case 2: ve->reg.vs2[addr>>1] = data; break;
	case 1: ve->reg.vs1[addr>>0] = data; break;
	default:
	    break;
    }
    return ve->reg.val[addr>>2];
}

static uint64_t ve_read(void *opaque, hwaddr addr, unsigned size)
{
    VEState *ve = VE(opaque);
    uint64_t data = ve_data_read(ve, addr, size);

    return data;
}

static void ve_write(void *opaque, hwaddr addr, uint64_t data, unsigned size)
{
    VEState *ve = VE(opaque);
    hwaddr offset = addr & ~3;

    ve_data_write(ve, addr, data, size);

    switch(offset) {
	case 0xa08:
	    if((VEREG(0xa08) & 1) == 0)
		qemu_irq_lower(ve->irq);
	    break;
	case 0xa0c:
	    if(VEREG(0xa0c) & 1) {
		VEREG(0xa0c) &= ~1;
		if((VEREG(0x000) & 0xf) == 0xa)
		    ve_isp_do_dma_copy(ve);
	    }
	    break;
	default:
	    break;
    }
}

static void ve_reset(DeviceState *dev)
{
    VEState *ve = VE(dev);
    int nvalues = sizeof(ve_reset_values) / sizeof(struct ve_reset_values);
    int n;

    for(n = 0; n < VE_NREG; n++)
	ve->reg.val[n] = 0;

    for(n = 0; n < nvalues; n++)
	ve->reg.val[ve_reset_values[n].offset>>2] = ve_reset_values[n].value;
}

static const MemoryRegionOps ve_ops = {
    .read	= ve_read,
    .write	= ve_write,
    .endianness	= DEVICE_NATIVE_ENDIAN,
};

static int ve_init(SysBusDevice *dev)
{
    VEState *ve = VE(dev);

    memory_region_init_io(&ve->iomem, OBJECT(ve), &ve_ops, ve, "vev.ve", VE_SIZE);
    sysbus_init_mmio(dev, &ve->iomem);
    sysbus_init_irq(dev, &ve->irq);
    return 0;
}

static void ve_class_init(ObjectClass *class, void *data)
{
    SysBusDeviceClass *sc = SYS_BUS_DEVICE_CLASS(class);
    DeviceClass *dc = DEVICE_CLASS(class);
    sc->init  = ve_init;
    dc->reset = ve_reset;
}

static const TypeInfo ve_type_info = {
    .name	    = TYPE_VE,
    .parent	    = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(VEState),
    .class_init	    = ve_class_init,
};

static void ve_type_init(void)
{
    type_register_static(&ve_type_info);
}

type_init(ve_type_init)

