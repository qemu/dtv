/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "hw/sysbus.h"
#include "device-common.h"
/* 
    This device doesn't aims to be accurate, only the minimum to make the
    software run.
*/
#define CCM_SIZE 0x400
#define CCM_NREG ((CCM_SIZE)/sizeof(uint32_t))
#define TYPE_CCM "vev-ccm"
#define CCM_STATE(obj) OBJECT_CHECK(CCMState, (obj), TYPE_CCM)

typedef struct CCMState {
    SysBusDevice    parent;
    MemoryRegion    iomem;
    uint32_t	    reg[CCM_NREG];
} CCMState;

static struct ccm_reset_val {
    hwaddr	offset;
    uint32_t	value;
} ccm_reset_vals[] = {
    {/* CCM_PLL4_CFG */ 0x018, 0x00001000 },
    {/* CCM_PLL5_CFG */ 0x020, 0xffffffff },
    {/* CCM_PLL6_CFG */ 0x028, 0xffffffff },
};

void ccm_device_init(hwaddr addr)
{
    DeviceState *dev;
    dev = qdev_create(NULL, TYPE_CCM);
    qdev_init_nofail(dev);
    sysbus_mmio_map(SYS_BUS_DEVICE(dev), 0, addr);
}

static uint64_t ccm_read(void *opaque, hwaddr addr, unsigned size)
{
    CCMState *s = CCM_STATE(opaque);
    uint64_t data = s->reg[addr>>2];
    return data;
}

static void ccm_write(void *opaque, hwaddr addr, uint64_t data, unsigned size)
{
    CCMState *s = CCM_STATE(opaque);
    s->reg[addr>>2] = data;
}

static const MemoryRegionOps ccm_device_ops = {
    .read	= ccm_read,
    .write	= ccm_write,
    .endianness	= DEVICE_NATIVE_ENDIAN,
    .impl = {
	.min_access_size = 4,
	.max_access_size = 4,
    },
};

static int ccm_init(SysBusDevice *dev)
{
    CCMState *s = CCM_STATE(dev);

    memory_region_init_io(&s->iomem, OBJECT(s), &ccm_device_ops, s, "vev.ccm", CCM_SIZE);
    sysbus_init_mmio(dev, &s->iomem);
    return 0;
}

static void ccm_reset(DeviceState *dev)
{
    CCMState *s = CCM_STATE(dev);
    int nvals = sizeof(ccm_reset_vals) / sizeof(struct ccm_reset_val);
    int n;
    for(n = 0; n < CCM_NREG; n++)
	s->reg[n] = 0;
    for(n = 0; n < nvals; n++)
	s->reg[ccm_reset_vals[n].offset>>2] = ccm_reset_vals[n].value;
}

static void ccm_class_init(ObjectClass *class, void *data)
{
    SYS_BUS_DEVICE_CLASS(class)->init = ccm_init;
    DEVICE_CLASS(class)->reset = ccm_reset;
}

static const TypeInfo ccm_info = {
    .name	    = TYPE_CCM,
    .parent	    = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(CCMState),
    .class_init	    = ccm_class_init,
};

static void ccm_type_init(void)
{
    type_register_static(&ccm_info);
}

type_init(ccm_type_init);

