/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "hw/sysbus.h"
#include "device-common.h"

/* Similar to empty_slot with added name and subregions. */

#define TYPE_DUMMY "dummy-device"
#define DUMMY(obj) OBJECT_CHECK(DummyDevice, (obj), TYPE_DUMMY)

typedef struct DummyDevice {
    SysBusDevice    parent;

    MemoryRegion    iomem;
    uint64_t	    size;
    char	    name[32];
} DummyDevice;

void *dummy_region_init(void *parent, const char *name, hwaddr addr, uint64_t size)
{
    DeviceState *dev;
    DummyDevice *d;

    dev = qdev_create(NULL, TYPE_DUMMY);
    d = DUMMY(dev);
    d->size = size;
    g_snprintf(d->name, sizeof(d->name), "dummy.%s", name);
    qdev_init_nofail(dev);

    if(parent)
	memory_region_add_subregion(&DUMMY(parent)->iomem, addr, &d->iomem);
    else
	sysbus_mmio_map(SYS_BUS_DEVICE(dev), 0, addr);
    return dev;
}

static uint64_t dummy_read(void *opaque, hwaddr addr, unsigned size)
{
    return 0;
}

static void dummy_write(void *opaque, hwaddr addr, uint64_t data, unsigned size)
{
}

static const MemoryRegionOps dummy_ops = {
    .read	= dummy_read,
    .write	= dummy_write,
    .endianness	= DEVICE_NATIVE_ENDIAN,
};

static int dummy_init(SysBusDevice *dev)
{
    DummyDevice *d = DUMMY(dev);

    if(!d->name[0])
	g_snprintf(d->name, sizeof(d->name), "dummy.unnamed");

    memory_region_init_io(&d->iomem, OBJECT(d), &dummy_ops, d, d->name, d->size);
    sysbus_init_mmio(dev, &d->iomem);
    return 0;
}

static void dummy_class_init(ObjectClass *class, void *data)
{
    SysBusDeviceClass *dc = SYS_BUS_DEVICE_CLASS(class);
    dc->init = dummy_init;
}

static const TypeInfo dummy_info = {
    .name	    = TYPE_DUMMY,
    .parent	    = TYPE_SYS_BUS_DEVICE,
    .instance_size  = sizeof(DummyDevice),
    .class_init	    = dummy_class_init,
};

static void dummy_type_init(void)
{
    type_register_static(&dummy_info);
}

type_init(dummy_type_init)

