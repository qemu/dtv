/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __DTV_COMMON_H__
#define __DTV_COMMON_H__

#include "exec/memory.h"

struct trace {
	int64_t		clock;
	MemoryRegion	*mr;
	uint64_t	addr;
	uint64_t	value;
	unsigned	size;
	char		type;
	struct trace	*next;
};

hwaddr dtv_memory_get_addr(MemoryRegion *mr);
bool   dtv_memory_is_direct(MemoryRegion *mr);

void dtv_trace_add(MemoryRegion *mr, uint64_t addr, uint64_t value, unsigned size, char type, int64_t clock);
void dtv_trace_destroy(void);
void dtv_filter_mr_add(MemoryRegion *mr);
void dtv_filter_mr_remove(MemoryRegion *mr);
void dtv_filter_init(void);

#endif
