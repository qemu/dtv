/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <gtk/gtk.h>
#include "dtv.h"

GtkWidget *dtv_construct_view_page(void)
{
    GtkWidget *page;
    GtkWidget *tmap;
    GtkWidget *view;

    page = gtk_hpaned_new();

    tmap = dtv_construct_trace_map();
    gtk_paned_pack1(GTK_PANED (page), tmap, FALSE, TRUE);

    view = dtv_construct_trace_view();
    gtk_paned_pack2(GTK_PANED (page), view, TRUE, TRUE);

    return page;
}

