/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <gtk/gtk.h>
#include "exec/memory.h"
#include "exec/address-spaces.h"
#include "dtv.h"

static GtkTreeStore *store = NULL;
enum {
    NAME_COLUMN,
    ADDR_COLUMN,
    TOGGLE_COLUMN,
    MEMORYREGION_COLUMN,
    COLOR_COLUMN,
    NUM_COLUMNS,
};

static void fill_store_with_memory(const MemoryRegion *mr, const hwaddr base, GtkTreeIter *parent)
{
    GtkTreeIter iter;
    gchar hexaddr[32];
    const MemoryRegion *sub;
    hwaddr start  = base + mr->addr;
    uint64_t size = int128_nz(mr->size) ? int128_get64(int128_sub(mr->size, int128_one())) : 0;
    hwaddr end    = start + size;

    g_snprintf(hexaddr, sizeof(hexaddr), "%08llx-%08llx", start, end);

    gtk_tree_store_append(store, &iter, parent);
    gtk_tree_store_set(store, &iter, NAME_COLUMN, mr->name,
                                     ADDR_COLUMN, hexaddr, MEMORYREGION_COLUMN, mr,
				     COLOR_COLUMN, "#AAA", -1);

    QTAILQ_FOREACH(sub, &mr->subregions, subregions_link) {
    	fill_store_with_memory(sub, start, &iter);
    }
}

static void mr_set_state(GtkTreeStore *store, GtkTreeIter *piter, gboolean active)
{
    MemoryRegion *mr;

    gtk_tree_model_get(GTK_TREE_MODEL (store), piter, MEMORYREGION_COLUMN, &mr, -1);
    if(dtv_memory_is_direct(mr))
    	return;

    gtk_tree_store_set(store, piter, TOGGLE_COLUMN, active, COLOR_COLUMN, active ? "#000" : "#AAA", -1);
    if(active)
        dtv_filter_mr_add(mr);
    else
        dtv_filter_mr_remove(mr);
}

static void mr_set_children(GtkTreeStore *store, GtkTreeIter *parent, gboolean active)
{
    GtkTreeIter iter;

    mr_set_state(store, parent, active);

    if(gtk_tree_model_iter_children(GTK_TREE_MODEL (store), &iter, parent)) {
        do {
            mr_set_children(store, &iter, active);
	} while(gtk_tree_model_iter_next(GTK_TREE_MODEL (store), &iter));
    }
}

static void cb_row_activated(GtkTreeView *tree, GtkTreePath *path, GtkTreeViewColumn *coln, gpointer data)
{
    GtkTreeIter iter;
    gboolean active;

    if(gtk_tree_model_get_iter(GTK_TREE_MODEL (store), &iter, path)) {
        gtk_tree_model_get(GTK_TREE_MODEL (store), &iter, TOGGLE_COLUMN, &active, -1);
	mr_set_children(store, &iter, active ? FALSE : TRUE);
    }
}

GtkWidget *dtv_construct_memory_page(void)
{
    GtkWidget *page;
    GtkWidget *tree;
    GtkCellRenderer *cell;
    GtkTreeViewColumn *coln;

    page = gtk_scrolled_window_new(NULL, NULL);

    store = gtk_tree_store_new(NUM_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_POINTER, G_TYPE_STRING);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE (store), ADDR_COLUMN, GTK_SORT_ASCENDING);

    tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL (store));
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW (tree), FALSE);
    gtk_container_add(GTK_CONTAINER (page), tree);
    g_signal_connect(G_OBJECT (tree), "row-activated", G_CALLBACK (cb_row_activated), NULL);

    cell = gtk_cell_renderer_text_new();
    g_object_set(G_OBJECT (cell), "font", "Monospace 8", NULL);

    coln = gtk_tree_view_column_new_with_attributes("", cell, "text", NAME_COLUMN, "foreground", COLOR_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW (tree), coln);

    coln = gtk_tree_view_column_new_with_attributes("", cell, "text", ADDR_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW (tree), coln);

    cell = gtk_cell_renderer_toggle_new();
    g_object_set(G_OBJECT (cell), "xalign", 1.0, NULL);

    coln = gtk_tree_view_column_new_with_attributes("", cell, "active", TOGGLE_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW (tree), coln);

    fill_store_with_memory(get_system_memory(), 0, NULL);
    fill_store_with_memory(get_unassigned_memory(), 0, NULL);
    gtk_tree_view_expand_all(GTK_TREE_VIEW (tree));

    dtv_filter_init();

    return page;
}

