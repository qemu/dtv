/*
 * Copyright (C) 2015  Manuel Braga <mbraga@openmailbox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "exec/memory.h"
#include "dtv-common.h"
#include "dtv.h"

static struct trace *traces = NULL;
static struct trace *lasttrace = NULL;
static MemoryRegion *lastmr = NULL;

static struct trace *new_trace(MemoryRegion *mr, uint64_t addr, uint64_t value, unsigned size, char type, int64_t clock)
{
	struct trace *t = g_new0(struct trace, 1);
	t->clock = clock;
	t->mr = mr;
	t->addr = addr;
	t->value = value;
	t->size = size;
	t->type = type;
	
	if(lasttrace)
		lasttrace->next = t;
	lasttrace = t;

	if(!traces)
		traces = t;

	return t;
}

void dtv_trace_add(MemoryRegion *mr, uint64_t addr, uint64_t value, unsigned size, char type, int64_t clock)
{
	struct trace *t = new_trace(mr, addr, value, size, type, clock);

	if(mr != lastmr) {
		lastmr = mr;

		dtv_trace_map_add(t);
	}

	dtv_trace_map_update();
}

void dtv_trace_destroy(void)
{
	struct trace *t = traces;
	struct trace *p;

	while(t) {
		p = t;
		t = t->next;
		g_free(p);
	}

	traces = NULL;
	lasttrace = NULL;
	lastmr = NULL;

	dtv_trace_map_clear();
}

