#!/usr/bin/env python
"""
DTV tracetool backend.
"""
__copyright__ = "Copyright (C) 2015 Manuel Braga <mbraga@openmailbox.org>"
__license__   = "GPL version 2 or (at your option) any later version"
from tracetool import out
PUBLIC = True

def generate_h_begin(events):
    out('#include "trace/control.h"',
        '#include "qemu/timer.h"',
        '#include "dtv/dtv-init.h"',
        '')
def generate_h(event):
    notnop = False
    if event.name.startswith("memory_region_"):
        notnop = True
    if notnop:
        name = event.name
	args = ", ".join(event.args.names())
	out('    if(trace_event_get_state(TRACE_%(nameid)s)) {',
	    '        dtv_%(func)s(qemu_clock_get_ms(QEMU_CLOCK_VIRTUAL), %(args)s);',
	    '    }',
	    nameid=name.upper(),
	    func=name,
	    args=args)
